<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;
use yii\web\Cookie;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'language'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'set-cookie', 'get-cookie'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        /*Yii::$app->MyComponent->hello();
        $lkrValue = Yii::$app->MyComponent->currencyConvert('USD','LKR',100);
        print_r($lkrValue);
        die();*/
        return $this->render('index');
    }

    public function actionLogin()
    {
        $this->layout = 'loginLayout';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSetCookie()
    {
        $cookie = new Cookie([
            'name'=>'test',
            'value'=>'haha',
        ]);

        Yii::$app->getResponse()->getCookies()->add($cookie);
    }

    public function actionGetCookie()
    {
        if (Yii::$app->getRequest()->getCookies()->has('test')){
            print_r(Yii::$app->getRequest()->getCookies()->getValue('test'));
        }
    }

    public function actionLanguage(){
        if (isset($_POST['lang'])){
            Yii::$app->language = $_POST['lang'];
            $cookie = new Cookie([
                'name'=>'lang',
                'value'=>$_POST['lang'],
            ]);
            Yii::$app->getResponse()->getCookies()->add($cookie);
        }
    }
}
