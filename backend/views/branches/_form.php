<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\models\Companies;

/* @var $this yii\web\View */
/* @var $model backend\models\Branches */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="branches-form">

    <?php $form = ActiveForm::begin([
        'id'=>$model->formName(),
        'enableAjaxValidation'=>true,
        'validationUrl'=>\yii\helpers\Url::toRoute('branches/validation'),
    ]); ?>

    <?= $form->field($model, 'companies_company_id')->dropDownList(
        ArrayHelper::map(Companies::find()->all(), 'company_id', 'company_name'),
        ['prompt'=>'Select Company']
    ); ?>

    <?= $form->field($model, 'branch_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'branch_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'branch_status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Status']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS

$('form#{$model->formName()}').on('beforeSubmit', function(e)
{
   var \$form = $(this);
    $.post(
        \$form.attr("action"), // serialize Yii2 form
        \$form.serialize()
    )
        .done(function(result) {
        if(result == 1)
        {
            //Tim cai id trong Modal::begin 'id' => 'modal' file index.php roi an no di
            $(document).find('#modal').modal('hide');
            $.pjax.reload({container:'#branchesGrid'});
        }else
        {
            $(\$form).trigger("reset");
            $("#message").html(result);
        }
        }).fail(function()
        {
            console.log("server error");
        });
    return false;
});

JS;
$this->registerJs($script);
?>

