<?php
return [
    'adminEmail' => 'admin@example.com',
    'languages' => [
        'vi'=>'vietnamese',
        'en'=>'english',
    ],
];
